# -*- encoding:utf-8 -*-


import json


class OverrideClass(object):

    def __init__(self, class_):
        self.class_ = class_

    def __enter__(self):
        return self.class_

    def __exit__(self, type, value, traceback):
        return isinstance(value, TypeError)


class Configs(object):

    @staticmethod
    def load(extension_name=None):
        temps = ''
        with open('extensions\\'+extension_name+'.json') as jsconf:
            temps = jsconf.read()

        tempj = json.loads(temps)
        return tempj['settings']


def _extension_template():

    py_template = """# -*- encoding:utf-8 -*-
# {0} extension

from farmlib import *
from extensionutils import *

# Extension code here
    """

    json_template = """{
    "exports":[
        "keyword1", "keyword2",
    ],
    "settings":{
        "keyword1":"Hello World!",
        "keyword2":"Lorem ipsum"
    }
}
    """

    print('== Extension Manager ==\n\n')
    print('-Extension template creator')

    ext_name = raw_input('Name of your extension: ')
    ext_name_f = (ext_name.replace('-', '_').replace(' ', '_')).lower()
    # check if exists

    with open(ext_name_f+'.py', 'w') as extn:
        extn.write(py_template.format(ext_name))

    with open(ext_name_f+'.json', 'w') as extj:
        extj.write(json_template)

    raw_input('Extension created')


if __name__ == '__main__':

    _extension_template()
