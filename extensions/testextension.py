# -*- encoding:utf-8 -*-

print('EXTENSIONS: Load test')

from farmlib import *
from extensionutils import *

configs = Configs.load('testextension')

with OverrideClass(SceneTitle) as title:
    def o_draw_game_title(self):
        print("OVERRIDE: "+configs['test'])
    title.draw_game_title = o_draw_game_title
    del o_draw_game_title
    del title
