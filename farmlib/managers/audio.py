# -*- encoding:utf-8 -*-


import pygame


class AudioManager:

    bgm_volume = 100
    bgs_volume = 100
    me_volume = 100
    se_volume = 100
    current_bgm = None
    current_bgs = None
    path = ''


    @classmethod
    def play_music(cls, bgm, pos):
        pygame.mixer.music.load(bgm)
        pygame.mixer.music.play(-1)


    @classmethod
    def play_bgm(cls, bgm, pos=0):
        if cls.is_current_bgm(bgm):
            cls.update_bgm_parameters(bgm)
        else:
            cls.play_music(bgm, pos)


    @classmethod
    def is_current_bgm(cls, bgm):
        return False

    @classmethod
    def update_bgm_parameters(cls, bgm):
        pass
