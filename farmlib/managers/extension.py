# -*- encoding:utf-8 -*-


class ExtensionManager:

    extensions = None

    @classmethod
    def load_extensions(cls):
        import extensions
        cls.extensions = extensions
