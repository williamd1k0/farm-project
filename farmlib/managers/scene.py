# -*- encoding:utf-8 -*-


import sys
import pygame
from pygame.locals import *
from ..core import Graphics


class SceneManager:

    scene = None
    next_scene = None
    stack = list()
    stopped = False
    scene_started = False
    exiting = False
    previous_class = None
    background_bitmap = None
    screen_width = 816
    screen_height = 480
    fps = 60
    accumulator = 0.0
    clock = pygame.time.Clock()


    @classmethod
    def init(cls):
        cls.init_graphics()

    @classmethod
    def init_graphics(cls):
        Graphics.init(cls.screen_width, cls.screen_height);


    @classmethod
    def run(cls, scene_class):
        cls.init()
        cls.goto(scene_class)
        cls.request_update()

    @classmethod
    def goto(cls, Scene_class=None):
        if Scene_class is not None:
            cls.next_scene = Scene_class()
        if cls.scene is not None:
            pass

    @classmethod
    def clear_stack(cls):
        cls.stack = list()


    @classmethod
    def tick_start(cls):
        Graphics.tick_start()


    @classmethod
    def tick_end(cls):
        Graphics.tick_end()


    @classmethod
    def update_main(cls):
        cls.change_scene()
        cls.update_scene()
        cls.render_scene()


    @classmethod
    def change_scene(cls):
        if cls.is_scene_changing() and not cls.is_current_scene_busy():
            if cls.scene is not None:
                cls.scene.terminate()
                cls.previous_class = cls.scene.__class__
            cls.scene = cls.next_scene
            if cls.scene is not None:
                cls.scene.create()
                cls.next_scene = None
                cls.scene_started = False
                cls.on_scene_create()
            if cls.exiting:
                cls.terminate()

    @classmethod
    def render_scene(cls):
        if cls.is_current_scene_started():
            pass
            # print('Debug: Renderando cena')
        elif cls.on_scene_loading():
            pass


    @classmethod
    def is_scene_changing(cls):
        return cls.exiting or cls.next_scene is not None


    @classmethod
    def is_current_scene_busy(cls):
        return cls.scene is not None and cls.scene.is_busy()

    @classmethod
    def is_current_scene_started(cls):
        return cls.scene is not None and cls.scene_started

    @classmethod
    def on_scene_create(cls):
        pass

    @classmethod
    def on_scene_start(cls):
        pass

    @classmethod
    def on_scene_loading(cls):
        pass

    @classmethod
    def update_scene(cls):
        if cls.scene is not None:
            if not cls.scene_started and cls.scene.is_ready():
                cls.scene.start()
                cls.scene_started = True
                cls.on_scene_start()
            if cls.is_current_scene_started():
                cls.scene.update()

    @classmethod
    def request_update(cls):
        while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    sys.exit()
            Graphics.frame_count += 1
            # print(Graphics.frame_count)
            cls.update()
            pygame.display.update()
            time_passed = cls.clock.tick(Graphics.FPS)

    @classmethod
    def update(cls):
        cls.tick_start()
        cls.update_main()
        cls.tick_end()
