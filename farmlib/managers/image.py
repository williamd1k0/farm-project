# -*- encoding:utf-8 -*-


import pygame
from ..core import Bitmap


class ImageManager:

    @classmethod
    def load_bitmap(cls, path, file_=None, hue=0, smooth=False):
        if file_ is None:
            return cls.load_empty_bitmap()
        else:
            bitmap = cls.load_normal_bitmap(path+file_+'.png', hue)
            bitmap.smooth = smooth
            return bitmap


    @classmethod
    def load_empty_bitmap(cls):
        return Bitmap()


    @classmethod
    def load_normal_bitmap(cls, path, hue):
        bitmap = Bitmap.load(path)
        return bitmap


    @classmethod
    def load_title1(cls, name, hue=0):
        return cls.load_bitmap('assets\\images\\titles1\\', name, hue)

    @classmethod
    def load_title2(cls, name, hue=0):
        return cls.load_bitmap('assets\\images\\titles2\\', name, hue)
