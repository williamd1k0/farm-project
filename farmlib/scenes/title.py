#-*- encoding:utf-8


import pygame
from base import SceneBase
from ..core import Graphics, Sprite
from ..managers import SceneManager, AudioManager, ImageManager


class SceneTitle(SceneBase):

    def __init__(self):
        super(SceneTitle, self).__init__()


    def init(self):
        super(SceneTitle, self).init()

    def create(self):
        super(SceneTitle, self).create()
        self.create_background()
        self.create_foreground()
        self.create_window_layer()
        self.create_command_window()


    def is_ready(self):
        if super(SceneTitle, self).is_ready():
            return True # for now
        else:
            return False

    def start(self):
        super(SceneTitle, self).start()
        SceneManager.clear_stack()
        self.center_sprite(self.back_sprite1)
        self.center_sprite(self.back_sprite2)
        self.play_title_music()
        self.start_fadein(self.fade_speed, None)

    def update(self):
        if not self.is_busy():
            # self.command_window.open()
            pass
        super(SceneTitle, self).update()
        self.font = pygame.font.SysFont('Arial', 40)
        Graphics.screen.blit(self.font.render('My new FARM Game', True, (0,0,0)), (210, 100))

    def is_busy(self):
        return super(SceneTitle, self).is_busy()

    def terminate(self):
        super(SceneTitle, self).terminate()
        # SceneManager snap for bg

    def create_background(self):
        self.back_sprite1 = Sprite(ImageManager.load_title1('Castle'))
        self.back_sprite2 = Sprite(ImageManager.load_title2('Medieval'))
        self.add_child(self.back_sprite1)
        #self.add_child(self.back_sprite2)

    def create_foreground(self):
        self.game_title_sprite = object()
        #self.add_child(self.game_title_sprite)
        if True: # draw gm title
            self.draw_game_title()

    def draw_game_title(self):
        # coordinates
        temp_name = 'My new FARM Game'
        # bitmap draw text
        print("DEBUG: Drawing Game Title - "+temp_name)


    def center_sprite(self, sprite):
        # center
        pass

    def create_command_window(self):
        self.command_window = object()
        # set command handlers
        #self.add_window(self.command_window)

    # window commands

    def play_title_music(self):
        print("DEBUG: Playing Title Music")
        AudioManager.play_bgm('Theme6.ogg')
