# -*- encoding:utf-8 -*-


from ..core import Stage
from ..managers import SceneManager


class SceneBase(Stage):

    fade_sprite = None
    window_layer = None

    def __init__(self):
        super(SceneBase, self).__init__()
        self.init()


    def init(self):
        self.active = False
        self.fade_sign = 0
        self.fade_duration = 0


    def create(self):
        print('DEBUG: Starting Scene '+str(self.__class__))

    def is_ready(self):
        return True

    def start(self):
        self.active = True

    def update(self):
        self.update_fade()
        self.update_children()

    def stop(self):
        self.active = False

    def is_busy(self):
        return self.fade_duration > 0;

    def terminate(self):
        pass

    def create_window_layer(self):
        self.window_layer = Stage() #'WindowLayer object'
        #self.add_child(self.window_layer)

    def add_window(self, window):
        self.window_layer.add_child(window)

    def start_fadein(self, duration, white):
        pass

    def start_fadeout(self, duration, white):
        pass

    def create_fade_sprite(self, white=None):
        if self.fade_sprite is None:
            self.fade_sprite = object()
            #self.add_child(self.fade_sprite)

        if white is None:
            pass
        else:
            pass

    def update_fade(self):
        pass

    def update_children(self):
        for child in self.children:
            child.update()

    def pop_scene(self):
        SceneManager.pop()

    def check_gameover(self):
        if False:
            SceneManager.goto(Scene_Gameover)

    def fade_out_all(self):
        pass

    def fade_speed(self):
        return 25

    def slow_fade_speed(self):
        return self.fade_speed() * 2
