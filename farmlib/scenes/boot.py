#-*- encoding:utf-8


from base import SceneBase
from title import SceneTitle
from ..managers import SceneManager
from datetime import datetime
import pygame


class SceneBoot(SceneBase):

    def __init__(self):
        super(SceneBoot, self).__init__()


    def init(self):
        super(SceneBoot, self).init()
        self.start_date = datetime.today()

    def create(self):
        super(SceneBoot, self).create()
        # data e config
        self.load_system_imgs()

    def load_system_imgs(self):
        # image manager init
        pass

    def is_ready(self):
        if super(SceneBoot, self).is_ready():
            return True # for now
        else:
            return False

    def is_gamefont_loaded(self):
        return True # for now

    def start(self):
        super(SceneBoot, self).start()
        self.check_player_location()
        SceneManager.goto(SceneTitle)
        self.update_game_title()

    def update_game_title(self):
        temp_name = 'My new FARM Game'
        pygame.display.set_caption(temp_name)

    def check_player_location(self):
        return None
