# -*- encoding:utf-8 -*-


class Rectangle:


    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    @staticmethod
    def empty_rectangle():
        return Rectangle(0,0,0,0)
    
