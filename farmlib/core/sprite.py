# -*- encoding:utf-8 -*-

from rect import Rectangle
from point import Point
from stage import Stage
from graphics import Graphics


class Sprite(Stage):

    _counter = 0

    def __init__(self, bitmap=None):
        self.frame = Rectangle.empty_rectangle()
        self.real_frame = Rectangle.empty_rectangle()
        self.offset = Point()
        self.blend_color = [0,0,0,0]
        self.color_tone = [0,0,0,0]
        self.tint_texture = None
        self._counter += 1
        self.sprite_id = self._counter
        self.opaque = False
        self.bitmap = bitmap
        self.position = Point()


    def update(self):
        Graphics.draw(self)
