# -*- encoding:utf-8 -*-

from rect import Rectangle
import pygame

class Graphics:

    frame_count = 0
    screen = None


    @classmethod
    def init(cls, width=800, height=600, fps=60):
        cls.screen = None
        cls.width = width
        cls.height = height
        cls.scale = 1
        cls.real_scale = 1
        cls.FPS = fps
        cls.init_render();


    @classmethod
    def init_render(cls):
        pygame.init()
        pygame.display.set_caption('FARM PY')
        cls.screen = pygame.display.set_mode((cls.width, cls.height), 0, 32)


    @classmethod
    def render(cls, stage):
        stage.update()


    @classmethod
    def draw(cls, sprite):
        cls.screen.blit(sprite.bitmap.img, (sprite.position.x, sprite.position.y))


    @classmethod
    def tick_start(cls):
        pass

    @classmethod
    def tick_end(cls):
        pass
