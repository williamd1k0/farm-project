# -*- encoding:utf-8 -*-


class Utils:


    FARM_NAME = 'PY'
    FARM_VERSION = (0,1,0)

    @staticmethod
    def rgbToCssColor(r,g,b):
        r = round(r)
        g = round(g)
        b = round(b)
        return 'rgb(' + r + ',' + g + ',' + b + ')'

    
