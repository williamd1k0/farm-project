#-*- encoding:utf-8


from rect import Rectangle
from point import Point


class Window(Stage):

    children = None


    def __init__(self):
        super(Window, self).__init__()
        self.is_window = True
        self.window_skin = None
        self.width = 0
        self.height = 0
        self.cursor_rect = Rectangle.empty_rectangle()
        self.openness = 255
        self.animation_count = 0

        self.padding = 18
        self.margin = 4
        self.color_tone = [0,0,0]

        self.window_sprite_container = None
        self.window_back_sprite = None
        self.window_cursor_sprite = None
        self.window_frame_sprite = None
        self.window_contents_sprite = None
        self.window_arrow_sprites = list()
        self.window_pausesign_sprite = None

        self.create_all_parts()

        self.origin = Point()
        self.active = True
        self.down_arrow_visible = False
        self.up_arrow_visible = False
        self.pause = False



    def update(self):
        if self.active:
            self.animation_count += 1
        for child in self.children:
            child.update()


    def move(self, x=0, y=0, width=0, height=0):
        self.x = x
        self.y = y
        if self.width != width or self.height != height:
            self.width = width
            self.height = height
            self.refresh_all_parts()


    def is_open(self):
        return self.openness >= 255


    def is_closed(self):
        return self.openness <= 0


    def set_cursor_rect(self, x=0, y=0, width=0, height=0):
        # floor
        self.cursor_rect.x = x
        self.cursor_rect.y = y
        self.cursor_rect.width = width
        self.cursor_rect.height = height
        self.refresh_cursor()


    def set_tone(self, r, g, b):
        self.tone = r, g, b
        self.refresh_back()

    def add_child_to_back(self, child):
        ctindex = self.children.index(self.window_sprite_container)
        return self.add_child_at(child, ctindex + 1)

    def update_transform(self):
        self.update_cursor()
        self.update_arrows()
        self.update_pausesign()
        self.update_contents()

    def create_all_parts(self):
        self.window_sprite_container = Stage()
        self.window_back_sprite = Sprite()
        self.window_cursor_sprite = Sprite()
        self.window_frame_sprite = Sprite()
        self.window_contents_sprite = Sprite()
        self.down_arrow_sprite = Sprite()
        self.up_arrow_sprite = Sprite()
        self.window_pausesign_sprite = Sprite()
        self.window_back_sprite.bitmap = Bitmap(1, 1)
        self.window_back_sprite.alpha = 192 / 255
        self.add_child(window_sprite_container)
        self.window_sprite_container.add_child(self.window_back_sprite)
        self.window_sprite_container.add_child(self.window_frame_sprite)
        self.add_child(self.window_cursor_sprite)
        self.add_child(self.window_contents_sprite)
        self.add_child(self.down_arrow_sprite)
        self.add_child(self.up_arrow_sprite)
        self.add_child(self.window_pausesign_sprite)
