# -*- encoding:utf-8 -*-


import dialogs
from graphics import Graphics
from utils import Utils
from stage import Stage
from point import Point
from rect import Rectangle
from bitmap import Bitmap
from sprite import Sprite
