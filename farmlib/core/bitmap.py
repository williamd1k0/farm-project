# -*- encoding:utf-8 -*-

import pygame
from rect import Rectangle

class Bitmap:


    def __init__(self, width=1, height=1):
        self.width = width
        self.height = height
        self.img = None
        self.src = None
        self.opacity = 255
        self.smooth = False
        self.font_face = 'Arial'
        self.font_size = 28
        self.font_italic = False
        self.text_color = 0xffffff
        self.outline_color = 0x000000
        self.outline_width = 4

    def get_rect(self):
        return Rectangle(0,0,self.width,self.height)


    def resize(self, width, height):
        pass


    def draw_text(self):
        pass


    @staticmethod
    def load(src):
        bitmap = Bitmap()
        bitmap.img = pygame.image.load(src).convert_alpha()
        bitmap.src = src
        return bitmap

    @staticmethod
    def snap():
        pass
