#-*- encoding:utf-8


import ctypes


def show_message(title='', message='', mode=0):
    return ctypes.windll.user32.MessageBoxA(None, message, title, 0+mode)

def show_error_message(title='', message=''):
    return show_message(title, message, 16)
