#-*- encoding:utf-8


class Stage(object):

    children = None


    def __init__(self):
        self.children = list()


    def add_child(self, child):
        self.children.append(child)


    def add_child_at(self, child, pos):
        pass


    def remove_child(self, child):
        for c_child in range(len(self.children)):
            if self.children[c_child] is child:
                del self.children[c_child]
                break


    def remove_child_at(self, pos):
        del self.children[pos]
