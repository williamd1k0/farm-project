#!/usr/bin/env python
#-*- encoding:utf-8

import sys
from farmlib import *

sys.path.append("extensions")

def main():
    ExtensionManager.load_extensions()
    SceneManager.run(SceneBoot)


if __name__ == '__main__':
    try:
        main()
    except Exception, e:
        dialogs.show_error_message('FARM ERROR!', str(e))
        sys.exit()
